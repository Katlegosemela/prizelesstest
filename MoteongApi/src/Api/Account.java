package Api;

/**
 * Author: Katlego Semela
 * Date : 04:28-16/01/202
 */

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class Account
 */
@WebServlet("/Account")
public class Account extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private  Connection connection = null;
	private final String databaseName ="";
	private final String url = "jdbc:mysql://localhost:3306/"+databaseName;
	private final String userName = "root";
	private final String passWord = "1234";
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Account() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());

        String emailAddress = request.getParameter("email");
		String mobileNumber = request.getParameter("num");
		String Password = request.getParameter("pass");
		HttpSession NewSession = request.getSession();

		try {
			Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
			try {
				connection = DriverManager.getConnection(url,userName,passWord);
				String query = "SELECT * FROM userdatabase.user";
				Statement stmnt = connection.createStatement();
				ResultSet results = stmnt.executeQuery(query);
				
				while(results.next())
				{
					String useremailAddress = results.getString("emailAddress");
					String usermobileNumber = results.getString("mobileNumber");
					String userPassword = results.getString("password");
					
					System.out.println(useremailAddress+usermobileNumber+userPassword);
					
					//login with either the email address or the mobile number
					
					if(((emailAddress.equals(useremailAddress))&&(Password.equals(userPassword)))||((mobileNumber.equals(usermobileNumber))&&(Password.equals(userPassword))))
					{           
			               NewSession.setAttribute("firstName",results.getString("firstName"));   	            	
			               request.getRequestDispatcher("Welcome.jsp").forward(request,response);  
					}
					else
					{
						request.getRequestDispatcher("SignUp.jsp").forward(request,response);
					}
				}
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}       

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		String firstName = request.getParameter("fName");
		String password =  request.getParameter("pass");
		String lastName = request.getParameter("lName");
		String emailAddress = request.getParameter("eAddress");
		String mobileNumber = request.getParameter("mNumber");
		     
		
        try {
		Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
		try {
			connection = DriverManager.getConnection(url,userName,passWord);
			PreparedStatement ps = connection.prepareStatement("INSERT INTO `userdatabase`.`user` (`emailAddress`, `firstName`, `mobileNumber`,`lastName`, `password`) VALUES(?,?,?,?,?);");
			ps.setString(1,emailAddress);
			ps.setString(2,firstName);
			ps.setString(3,mobileNumber); 
			ps.setString(4,lastName);
			ps.setString(5,password);
			  
			int status = ps.executeUpdate();
			   
			if(status!=0)
			{  
				System.out.println("Connection established, record inserted");
				request.getRequestDispatcher("Login.jsp").forward(request,response);
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	} catch (InstantiationException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (IllegalAccessException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (ClassNotFoundException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
        System.out.println(firstName);
}
	
		
	}   

