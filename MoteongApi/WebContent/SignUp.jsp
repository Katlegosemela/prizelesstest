<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Sign Up</title>    
	<meta charset="UTF-8">  
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="Asserts/images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="Asserts/vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="Asserts/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="Asserts/fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="Asserts/vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="Asserts/vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="Asserts/vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="Asserts/vendor/select2/select2.min.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="Asserts/vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="Asserts/css/util.css">
	<link rel="stylesheet" type="text/css" href="Asserts/css/main.css">
<!--===============================================================================================-->
</head>
<body>
	
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100 p-l-85 p-r-85 p-t-55 p-b-55">
				<form class="login100-form validate-form flex-sb flex-w" action="Account" method="post">
					<span class="login100-form-title p-b-32">
						sign up
					</span>

					<span class="txt1 p-b-11">
						First Name
					</span>
					<div class="wrap-input100 validate-input m-b-36" data-validate = "First Name is required">
						<input class="input100" type="text" name="fName">
						<span class="focus-input100"></span>
					</div>
					
					<span class="txt1 p-b-11">
						Last Name
					</span>
					<div class="wrap-input100 validate-input m-b-36" data-validate = "Last Name is required">
						<input class="input100" type="text" name="lName">
						<span class="focus-input100"></span>
					</div>
					
					<span class="txt1 p-b-11">
						Mobile Number
					</span>
					<div class="wrap-input100 validate-input m-b-36" data-validate = "Mobile Number is required">
						<input class="input100" type="text" name="mNumber">
						<span class="focus-input100"></span>
					</div>
					
				    <span class="txt1 p-b-11">
						Email Address
					</span>
					<div class="wrap-input100 validate-input m-b-36" data-validate = "Email Address is required">
						<input class="input100" type="email" name="eAddress">
						<span class="focus-input100"></span>
					</div>
					
					<span class="txt1 p-b-11">
						Password
					</span>
					<div class="wrap-input100 validate-input m-b-36" data-validate = "Password is required">
						<span class="btn-show-pass">
							<i class="fa fa-eye"></i>
						</span>
						<input class="input100" type="password" name="pass">
						<span class="focus-input100"></span>
					</div>
					 
					<div class="container-login100-form-btn"> 
                        <input type="submit" name="register" value="register" onclick=""/>
					</div>

				</form>
			</div>
		</div>
	</div>
		
	<script src="Asserts/js/signUp.js"></script>	
<!--===============================================================================================-->
	<script src="Asserts/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="Asserts/vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="Asserts/vendor/bootstrap/js/popper.js"></script>
	<script src="Asserts/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="Asserts/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="Asserts/vendor/daterangepicker/moment.min.js"></script>
	<script src="Asserts/vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="Asserts/vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="Asserts/js/main.js"></script>
    <script src="Asserts/js/signUp.js"></script>

</body>
</html>